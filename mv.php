<?php
/**
 * Multiview CSV Importer
 *
 * Imports product level pricing from .csv's based on SKU numbers.
 *
 * @package   Multiview_CSV_Importer
 * @author    Jeff Sagal <jeff@sagalbot.com>
 * @license   GPL-2.0+
 * @link      http://code.sagalbot.com
 * @copyright 2014 Jeff Sagal
 *
 * @wordpress-plugin
 * Plugin Name:       Multiview CSV Importer
 * Plugin URI:        http://code.sagalbot.com
 * Description:       Imports product level pricing from .csv's based on SKU numbers.
 * Version:           0.0.1
 * Author:            Jeff Sagal
 * Author URI:        http://sagalbot.com
 * Text Domain:       mv
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * GitHub Plugin URI: https://github.com/sagalbot/NI-MV-CSV
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-mv.php' );

/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
register_activation_hook( __FILE__, array( 'Multiview_CSV_Importer', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Multiview_CSV_Importer', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'Multiview_CSV_Importer', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * @TODO:
 *
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-mv-admin.php' );
	add_action( 'plugins_loaded', array( 'Multiview_CSV_Importer_Admin', 'get_instance' ) );

	//  Composer Includes
	require 'vendor/autoload.php';
}
