<?php
/**
 * Multiview_CSV_Importer
 *
 * @package   Multiview_CSV_Importer_Admin
 * @author    Jeff Sagal <jeff@sagalbot.com>
 * @license   GPL-2.0+
 * @link      http://code.sagalbot.com
 * @copyright 2014 Jeff Sagal
 */

/**
 * Multiview_CSV_Importer_Admin class. This class should ideally be used to work with the
 * administrative side of the WordPress site.
 *
 * @package Multiview_CSV_Importer_Admin
 * @author  Jeff Sagal <jeff@sagalbot.com>
 */
class Multiview_CSV_Importer_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	public $id = null; // CSV attachment ID

	/**
	 * Initialize the plugin by loading admin scripts & styles and adding a
	 * settings page and menu.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		/*
		 * Call $plugin_slug from public plugin class.
		 */
		$plugin = Multiview_CSV_Importer::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Add the options page and menu item.
		// add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Add an action link pointing to the options page.
		$plugin_basename = plugin_basename( plugin_dir_path( realpath( dirname( __FILE__ ) ) ) . $this->plugin_slug . '.php' );
		add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'add_action_links' ) );

		add_action( 'init', array( $this, 'register_mv_importer' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $this->plugin_screen_hook_suffix == $screen->id ) {
			wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'assets/css/admin.css', __FILE__ ), array(), Multiview_CSV_Importer::VERSION );
		}

	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $this->plugin_screen_hook_suffix == $screen->id ) {
			wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), Multiview_CSV_Importer::VERSION );
		}

	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		/*
		 * Add a settings page for this plugin to the Settings menu.
		 *
		 * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
		 *
		 *        Administration Menus: http://codex.wordpress.org/Administration_Menus
		 *
		 * @TODO:
		 *
		 * - Change 'manage_options' to the capability you see fit
		 *   For reference: http://codex.wordpress.org/Roles_and_Capabilities
		 */
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Multiview CSV Importer Settings', $this->plugin_slug ),
			__( 'Multiview CSV Importer', $this->plugin_slug ),
			'manage_options',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once( 'views/admin.php' );
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_slug ) . '">' . __( 'Settings', $this->plugin_slug ) . '</a>'
			),
			$links
		);

	}

	/**
	 * Registers the importer on import.php
	 */
	public function register_mv_importer() {
		//  This function is only loaded on /wp-admin/import.php
		if( function_exists('register_importer') ) {
			register_importer( 'multiview', 'Multiview CSV', 'Imports level pricing for products.', array( $this, 'dispatch' ) );
		}
	}

	/**
	 * Handles the CSV upload and initial parsing of the file to prepare for
	 * displaying author import options.
	 * 
	 * Borrowed from wordpress-importer.php
	 *
	 * @return bool False if error uploading or invalid file, true otherwise
	 */
	public function handle_upload() {
		$file = wp_import_handle_upload();

		if ( isset( $file['error'] ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br />';
			echo esc_html( $file['error'] ) . '</p>';
			return false;
		} else if ( ! file_exists( $file['file'] ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br />';
			printf( __( 'The export file could not be found at <code>%s</code>. It is likely that this was caused by a permissions problem.', 'wordpress-importer' ), esc_html( $file['file'] ) );
			echo '</p>';
			return false;
		}

		$this->id = (int) $file['id'];
		$import_data = $this->parse( $file['file'] );
		if ( ! is_object( $import_data ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br /></p>';
			return false;
		}

		return true;
	}

	/**
	 * Parse a CWV file
	 *
	 * @param string $file Path to CWV file for parsing
	 * @return object Keboola CSV
	 */
	public function parse( $file ) {
		$csvFile = new Keboola\Csv\CsvFile( $file );
		return $csvFile;
	}

	/**
	 * The main controller for the actual import stage.
	 *
	 * @param string $file Path to the WXR file for importing
	 */
	public function import( $file ) {
		if ( ! is_file($file) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br />';
			echo __( 'The file does not exist, please try again.', 'wordpress-importer' ) . '</p>';
			$this->footer();
			die();
		}

		$csv = $this->parse( $file );

		foreach( $csv as $row ) {

			$sku      = $row[1];
			$title    = $row[2];
			$price    = (float) $row[4];
			$currency = $row[6];

			//  Determine the price level
			switch ( $row[3] ) {
				case 'LEVEL1':
					$price_level = 1;
					break;
				case 'LEVEL2':
					$price_level = 2;
					break;
				case 'LEVEL3':
					$price_level = 3;
					break;
				case 'LEVEL4':
					$price_level = 4;
					break;
				case 'LEVEL5':
					$price_level = 5;
					break;
				case 'LEVEL6':
					$price_level = 6;
					break;
				case 'LEVEL7':
					$price_level = 7;
					break;
				case 'LEVEL8':
					$price_level = 8;
					break;
				case 'LEVEL9':
					$price_level = 9;
					break;
				case 'LEVEL10':
					$price_level = 10;
					break;
			}

			$product = $this->get_product_by_sku( $sku );
			if( $product ) {

				//  Set Standard Price
				if( $price_level == 1 ) {

					//  Update _price & _regular_price fields
					update_post_meta( $product->id, '_price', $price );
					update_post_meta( $product->id, '_regular_price', $price );

					$_product = get_product( $product->id );
					echo "Price level 1 set at <strong>" . $_product->get_price() . "</strong> for " . $_product->get_title();
				} else {

					//  Update all other price levels
					update_post_meta( $product->id, '_price_level_' . $price_level, $price );

					$_product = get_product( $product->id );
					echo "Price level " . $price_level . " set at <strong>" . $_product->get_price() . "</strong> for " . $_product->get_title();
				}

			}
		}
	}

	/**
	 * Registered callback function for the WordPress Importer
	 * Manages the three separate stages of the WXR import process
	 * Borrowed from wordpress-importer.php
	 */
	public function dispatch() {

		$this->header();

		$step = empty( $_GET['step'] ) ? 0 : (int) $_GET['step'];
		switch ( $step ) {
			case 0:
				$this->greet();
				break;
			case 1:
				if( $this->handle_upload() ) {
					check_admin_referer( 'import-upload' );
					$file = get_attached_file( $this->id );
					set_time_limit(0);
					$this->import( $file );
				}
				break;
		}

		$this->footer();
	}

	/**
	 * Display introductory text and file upload form
	 * Borrowed from wordpres-importer.php
	 */
	public function greet() {
		echo '<div class="narrow">';
		echo '<p>'.__( 'Howdy! Upload your MultiView CSV file and we&#8217;ll import the level pricing.', 'wordpress-importer' ).'</p>';
		echo '<p>'.__( 'Choose a CSV (.csv) file to upload, then click Upload file and import.', 'wordpress-importer' ).'</p>';
		wp_import_upload_form( 'admin.php?import=multiview&amp;step=1' );
		echo '</div>';
	}

	/**
	 * Display import page title
	 * Borrowed from wordpres-importer.php
	 */
	public function header() {
		echo '<div class="wrap">';
		screen_icon();
		echo '<h2>' . __( 'Import MultiView Pricing', 'wordpress-importer' ) . '</h2>';

		$updates = get_plugin_updates();
		$basename = plugin_basename(__FILE__);
		if ( isset( $updates[$basename] ) ) {
			$update = $updates[$basename];
			echo '<div class="error"><p><strong>';
			printf( __( 'A new version of this importer is available. Please update to version %s to ensure compatibility with newer export files.', 'wordpress-importer' ), $update->update->new_version );
			echo '</strong></p></div>';
		}
	}

	/**
	 * Close div.wrap
	 * Borrowed from wordpres-importer.php
	 */
	public function footer() {
		echo '</div>';
	}

	/**
	 * Retrieve products from the DB based on the SKU
	 * @param  mixed $sku String/Int representing SKU
	 * @return object|null WC_Product
	 */
	public function get_product_by_sku( $sku ) {
		global $wpdb;
		$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );

		if ( $product_id ) return get_product( $product_id );
		return null;
	}


}
