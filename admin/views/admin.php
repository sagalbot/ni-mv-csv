<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   Multiview_CSV_Importer
 * @author    Jeff Sagal <jeff@sagalbot.com>
 * @license   GPL-2.0+
 * @link      http://code.sagalbot.com
 * @copyright 2014 Jeff Sagal
 */
?>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<!-- @TODO: Provide markup for your options page here. -->

</div>
