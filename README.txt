=== Multiview CSV Importer ===
Contributors: sagalbot
Donate link: http://sagalbot.com/
Tags: csv, import
Requires at least: 3.9
Tested up to: 3.9
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Imports product level pricing from .csv's based on SKU numbers.