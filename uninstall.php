<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   Multiview_CSV_Importer
 * @author    Jeff Sagal <jeff@sagalbot.com>
 * @license   GPL-2.0+
 * @link      http://code.sagalbot.com
 * @copyright 2014 Jeff Sagal
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// @TODO: Define uninstall functionality here
